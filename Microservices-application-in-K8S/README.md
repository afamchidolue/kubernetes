mkdir online-shop-microservices
cd online-shop-microservices
create a linode kubernetes Engine with 2 node
download the kube config file
chmod 400 ~/Downloads/online-shop-kubeconfig.yaml
ls -l  ~/Downloads/online-shop-kubeconfig.yaml
export KUBECONFIG=~/Downloads/online-shop-kubeconfig.yaml
kuebectl get node
kubectl create ns microservices
kubectl apply -f config.yaml -n microservices
kubectl get pod -n microservices
kubectl get svc -n microservices
go to the ip adress of the node in LKE
go the browser and <node-ip-address>:30007
