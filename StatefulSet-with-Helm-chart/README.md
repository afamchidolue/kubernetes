create a cluster label in the Linode Kubernetes Engine
download the Kubeconfig file
set the kubeconfig as a environment variable      <export KUBECONFIG=test-kubeconfig.yaml>
### use Helm chart to create a kubernetes cluster
 helm repo add bitnami https://charts.bitnami.com/bitnami
 helm search repo bitnami/mongo
 helm install mongodb --values test-mongodb-values.yaml bitnami/mongodb
kubectl get pod
kubectl get all
kubectl get secret
kubectl get secret mongodb -o yaml
## create a mongoExpress
kubectl apply -f test-mongo-express.yaml
### use Helm chart to create a nginx ingress  controller
helm search repo bitnami/nginx
 helm install nginx-ingress bitnami/nginx-ingress-controller
## create the ingress rule
go to linode NodeBalancer copy the hostname  and paste the host section of the yaml file
 kubectl apply -f test-ingress.yaml
